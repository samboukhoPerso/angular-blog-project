import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  
  firstDate = new Date();
  secondDate = new Date();
  thirdDate = new Date();

  
  posts= [{
    title: 'Mon premier post',
    content: 'Le code complet et fonctionnel doit être déposé dans un dépôt Git en ligne que les validateurs doivent pouvoir cloner. Vous fournirez le lien vers le dépôt dans le champs de commentaires. Le projet doit être fonctionnel une fois que le projet a été cloné et que la commande npm install a été saisie à la racine.',
    loveIts: 1,
    created_at: this.firstDate 
  }, {
      title: 'Mon second post',
      content: 'Le code complet et fonctionnel doit être déposé dans un dépôt Git en ligne que les validateurs doivent pouvoir cloner. Vous fournirez le lien vers le dépôt dans le champs de commentaires. Le projet doit être fonctionnel une fois que le projet a été cloné et que la commande npm install a été saisie à la racine.',
      loveIts: -1,
      created_at: this.secondDate
    }, {
      title: 'Encore un post',
      content: 'Le code complet et fonctionnel doit être déposé dans un dépôt Git en ligne que les validateurs doivent pouvoir cloner. Vous fournirez le lien vers le dépôt dans le champs de commentaires. Le projet doit être fonctionnel une fois que le projet a été cloné et que la commande npm install a été saisie à la racine.',
      loveIts: 0,
      created_at: this.thirdDate
    } ]
}
